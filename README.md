# MagiskHideDenyBackup

*Inspired by osm0sis from XDA*

A simple module to backup any apps you have in your MagiskHide or Deny lists for easy restore on new ROM flash

Backs up items from MagiskHide List or Deny List to /sdcard/MagiskList.txt

**PLEASE NOTE:** 

- We (myself, Osm0sis, ipdev and pndwal) all tried to get this backup and restore functionality (along with backup/restore for the superuser list - i have modules for that too - see here: [Related Modules](#related-modules)) added natively to Magisk Manager via Magisk's Github, lets just say it was rejected, so here we are...

- These/There are **TWO** very distinct modules that work together, one backs up ([MagiskHideDenyBackup](https://gitlab.com/adrian.m.miller/magiskhidedenybackup)),
and one restores ([MagiskHideDenyRestore](https://gitlab.com/adrian.m.miller/magiskhidedenyrestore)). 
You need **BOTH** modules. 

For the terminally lazy (like me), ive created a couple of simple magisk modules to backup and restore the 
packages and process you may have hidden via Magisk's MagiskHide (old magisk/custom) or Deny List (canary/alpha). 

In the old days, a trick (to essentially not have to retick all packages and process to hide - especially handy 
if you wanted to have the packages hidden before restoring your apps via something like Migrate, to make sure 
apps never had a chance to see root) was to simply backup /data/adb/magisk.db and restore it on ROM flashes. 
but with magisk.db schema changes this has now ceased to be a safe easy trick. 

So the way now (inspired by osm0sis from XDA) is to dump the list of packages and processes via the 

```magiskhide ls (or magisk --deny ls)```

command to a text file and reimport it after a new ROM flash (assuming you have kept a copy off device if you do 
have to format data).

---

### Module Installation: ###

- Download from **[Releases](https://gitlab.com/adrian.m.miller/magiskhidedenybackup/-/releases)**  
![](https://gitlab.com/adrian.m.miller/magiskhidedenybackup/-/badges/release.svg)
- Install the module via Magisk app/Fox Magisk Module Manager/MRepo
- Reboot

---

### Usage: ###

1) Install MagiskHideDenyBackup module

    This:

    - Backs up your magiskhide (or magisk deny) list to /sdcard/MagiskList.txt
    - Writes a log file to /sdcard/MagiskHideDenyExport.log
	
2) Copy /sdcard/MagiskList.txt off device for safe keeping
	
The module will remain installed, unless removed, after the process completes.

The only active file in the entire module is /common/install.sh, and it is commented.

It is safe to leave installed and ignored if you like. You can always flash it
again at any time to update /sdcard/MagiskList.txt 

---

#### **To restore your backup after a new ROM flash etc, see the [MagiskHideDenyRestore Module Repo](https://gitlab.com/adrian.m.miller/magiskhidedenyrestore) for the partner module to restore your backup**

---

### Changelog ###

Please see: https://gitlab.com/adrian.m.miller/magiskhidedenybackup/-/blob/main/changelog.md
           
---

### **Related modules:**

SuperuserListBackup: https://gitlab.com/adrian.m.miller/superuserlistbackup

SuperuserListRestore: https://gitlab.com/adrian.m.miller/superuserlistrestore

---


